// 由于每个页面都需要显示购物车的数量，为了防止重复代码所以使用mixins混入，在main.js全局使用

import {mapGetters} from 'vuex'

export default {
	computed:{
		...mapGetters('m_cart',['total'])
	},
	watch:{
		// 监听total的变化 重新渲染徽标
		total(){
			this.setBadge()
		}
	},
	// 页面刚展示时加载徽标
	onShow() {
		this.setBadge()
	},
	methods:{
		setBadge(){
			uni.setTabBarBadge({
				index:2,  //tabbar购物车的索引
				text:this.total + ''
			})
		}
	}
}