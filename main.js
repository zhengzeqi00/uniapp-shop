
// #ifndef VUE3
import Vue from 'vue'
import App from './App'
import store from '@/store/store.js'
// 导入tabbar-badge 混入代码
import tabbarBadge from '@/mixins/tabbar-badge.js'
//  引入网络请求包
import {$http} from '@escook/request-miniprogram'

// 全局混入mixin
Vue.mixin(tabbarBadge)

uni.$http = $http
// 请求根路径
$http.baseUrl = 'https://api-hmugo-web.itheima.net'

// 请求拦截器  即请求前
$http.beforeRequest = function(options){
	uni.showLoading({
		title:"数据加载中...."
	})
	// 判断当前请求是否为包含my的接口
	if(options.url.indexOf('/my/') !== -1){
		// 为请求头添加身份认证字段
		options.header = {
			Authorization:store.state.m_user.token
		}
	}
}

// 响应拦截器 即请求后
$http.afterRequest = function(options){
	uni.hideLoading()
}

// ****封装请求数据弹窗****
uni.$showMsg = function(title="数据请求失败",duration=2000){
	uni.showToast({
		title,
		duration,
		icon:'none'
	})
}

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App,
	store
})
app.$mount()
// #endif







// #ifdef VUE3
import { createSSRApp } from 'vue'
import App from './App.vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif