// user.js用于存放address信息

export default {
	namespaced:true,
	// 数据
	state:()=>({
		address:JSON.parse(uni.getStorageSync('address') || '{}'),
		token:uni.getStorageSync('token') || '',
		userinfo:JSON.parse(uni.getStorageSync('userinfo') || '{}'),
		redirectInfo:null
	}),
	// 方法
	mutations:{
		// 更新收货地址
		updateAddress(state,address){
			state.address = address
			this.commit('m_user/saveStorageAddress')
		},
		// 把Address保存到本地
		saveStorageAddress(state){
			uni.setStorageSync('address',JSON.stringify(state.address))
		},
		// 更新userinfo
		updateUserInfo(state,userinfo){
			state.userinfo = userinfo
			this.commit('m_user/saveUserInfoStorage')
		},
		// 把userinfo保存在本地
		saveUserInfoStorage(state){
			uni.setStorageSync('userinfo',JSON.stringify(state.userinfo))
		},
		// 更新token
		updateToken(state,token){
			state.token = token
			this.commit('m_user/saveTokenStorage')
		},
		saveTokenStorage(state){
			uni.setStorageSync('token',state.token)
		},
		// 更新重定向信息
		updateRedirectInfo(state,info){
			state.redirectInfo = info
		}
	},
	
	getters:{
		addressStr(state){
			if(!state.address.provinceName) return ''
			return state.address.provinceName + state.address.cityName 
					+ state.address.countyName + state.address.detailInfo
		}
	}
}