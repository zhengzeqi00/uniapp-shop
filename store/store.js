import Vue from 'vue'	//引入vue
import Vuex from 'vuex'		//引入vuex
import moduleCart  from '@/store/cart.js'
import moduleUser from '@/store/user.js'
// 使用vuex
Vue.use(Vuex)

const store = new Vuex.Store({
	modules:{
		'm_cart':moduleCart,
		'm_user':moduleUser
	}
})

export default store