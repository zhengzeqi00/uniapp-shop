// 购物车store模块

// 初始化vuex模块
export default {
	namespaced:true,
	state:()=>({
		// 购物车数组，用来存储购物车中每个商品的信息对象 
		// 每个商品信息对象包含{goods_id，goods_name，goods_price，goods_count，goods_small_logo，goods_state}
		cart: JSON.parse(uni.getStorageSync('cart') || '[]')
	}),
	mutations:{
		// 保存到本地存储
		saveStorage(state){
			uni.setStorageSync('cart',JSON.stringify(state.cart))
		},
		// 添加商品信息到cart数组
		addToCart(state,goods){
			// find方法返回undefined和对应的数据
			const findResult = state.cart.find(x=> x.goods_id === goods.goods_id)
			// !findResult 表示为undefined 不存在对应的数据所以push
			if(!findResult){
				state.cart.push(goods)
			}else{
				// 存在则商品数量加1
				findResult.goods_count ++
				this.commit('m_cart/saveStorage')
			}
		},
		// 更新购物车商品勾选状态
		updateGoodsState(state,goods){
			// 查询是否存在该商品
			const findResult = state.cart.find(x=>x.goods_id === goods.goods_id)
			// 存在的话则重新给state赋值
			if(findResult){
				findResult.goods_state = goods.goods_state
				this.commit('m_cart/saveStorage')
			}	
		},
		//	更新商品数量
		updateGoodsCount(state,goods){
			const findResult = state.cart.find(x=>x.goods_id === goods.goods_id)
			if(findResult){
				findResult.goods_count = goods.goods_count
				this.commit('m_cart/saveStorage')
			}	
		},
		// 根据ID删除商品
		removeGoodsById(state,goods_id){
			// filter 过滤数组内部的id不等于传入的goods_id则保存下来，否则不保存
			state.cart = state.cart.filter(x => x.goods_id != goods_id)
			this.commit('m_cart/saveStorage')
		},
		// 更新所有商品的勾选状态
		updateAllGoodsState(state,newState){
			state.cart.forEach(x => x.goods_state = newState)
			this.commit('m_cart/saveStorage')
		},

	},
	getters:{
		// 实现统计购物车商品的总数量
		total(state){
			// let c = 0
			// // x表示每一件商品
			// state.cart.forEach(x => c += x.goods_count)
			// return c
			return state.cart.reduce((total,item)=>total += item.goods_count,0)
		},
		// 购物车中已勾选的商品数量
		checkedCount(state){
			//先使用filter过滤已勾选的商品，再使用reduce将已勾选的商品进行累加
			return state.cart.filter(x=>x.goods_state).reduce((total,item)=>total += item.goods_count,0)
		},
		// 计算所有商品的价格
		checkedGoodsAmount(state){
			// 计算商品的数量和价格相乘 获得总价格
			return state.cart.filter(x=>x.goods_state).reduce((total,item)=>total += item.goods_count*item.goods_price,0).toFixed(2)
		}
	}
}